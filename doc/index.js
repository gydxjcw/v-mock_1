const fs = require('fs');
const path = require('path');
const Mock = require('mockjs');

// Check if the data.json file path is provided as a command-line argument
if (process.argv.length < 3) {
    console.error('Usage: node main.js <data_json_path>');
    process.exit(1);
}

const dataJsonPath = process.argv[2];

// Read the data from the provided file path
fs.readFile(dataJsonPath, 'utf8', (err, jsonData) => {
    if (err) {
        console.error(`Error reading ${dataJsonPath}:`, err);
        return;
    }

    try {
        const data = JSON.parse(jsonData);
        // Use Mock.mock with the data read from the file
        const result = Mock.mock(data);

        // Generate a random file name using the current timestamp and a random number
        const randomFileName = `${Date.now()}-${Math.floor(Math.random() * 10000)}.json`;
        const outputFilePath = path.join(__dirname, randomFileName);
        
        // Write the result to the output file
        fs.writeFile(outputFilePath, JSON.stringify(result, null, 4), 'utf8', (err) => {
            if (err) {
                console.error(`Error writing to ${randomFileName}:`, err);
                return;
            }

            console.log(`${randomFileName}`);
        });
    } catch (parseError) {
        console.error(`Error parsing ${dataJsonPath}:`, parseError);
    }
});

# 背景
原项目信息克隆来自 https://gitee.com/vtDev/v-mock 由于仓库作者很久未更新，根据自己的需要新增了一些功能 具体看更新日志 有需要的可以拿去用
有其他小伙伴做了改造的欢迎提交代码贡献，本仓库会持续维护

# 更新日志
## 20230723
1. 新增接口响应内容支持自定义python脚本执行，适用场景可以编写请求代理进行转发请求，比如开发做功能测试的时候需要验证接口返回信息是错误的情况，正常情况下不会出现，模拟困难可以通过v-mock配置模拟响应数据，没问题的话再转发真实的接口请求，使用方式如下
新增请求响应示例，描述信息需要携带python关键字 比如测试python脚本执行
响应内容填写如下 根据自己需要改写url method headers，注意需要有python3环境并且安装了requests依赖
```
import json
import sys

import requests

url = "http://localhost/system/response/list"

method = "POST"

data = {}

headers = {'cookie': 'rememberMe=70qRu7y59T17AOWJB5F+sXPQdelwV1gAil4fsgekCU8r6wyrDBXWnqCwKipiT3DQx2s0c6wDlL9L4NyTv9kxymL+myminxMDOXDY2R48pHs4NG6RyxDeOrjWZ0jOZI2E88850n6iD84oSgjkK6xKmAE60udD5o4qkBiQaUg6emCcPK9E21ldt1w6USNONuhfete9DUqvtJiKRslOOEU0+9/5Qu7E1FCKM3Tj39E8iMquco7Mb9VKUXgB7guVN+L4Zt2To2nbfhUsPqDSSMFbY7djzc6/Z2tLRlsm/hVawuDNB6RnGyv93O1deNKXJr9t9XPtwp/Izi12lQImAXFvX1r6k8bfF4cvD1YvsLBycbPHMZUCrhjIl0rXZCOiKVf/EK60ZY+oVAl8FmtAqi2+9lp/yN7bICgHbqhv7OmMWpme/OsBi0LYeFR/4fqJvg8PUuM3FcqgIIfDhe7D5FkcpAE7VPww+5yc/w/0qKo0ZT0bL6yocpsayA7JSJUCXQOhIWCqLPflFdWmTGpYIhDTr//FV1UV3w1u0FGWUz3Dxi9iCylQhUr/v1WL2fYgygmZzt6t0HwJC4CfxrFOmQco9QdOJJMyyyLNXH2X+FGbMuyfPRRURxyZ4NpSjdK4ocRSLU2w9rChTvB30GtF7zk/7+IN62RLL202RxPuaiBWRmbSNw5S2mNR162l0rgCA0t2Y27owi9H8GDBkHFL1V3uLCMQ67n1AoosfGLyKFuDB7XN9HPmF/w+1adHIo4y+CWQYoyYnBj2ldRhSAW5F9MyZgfbyYG1H/aM6klJUv64af91wG17VYhngaqR6fM77q/xEO39hGfBTEwrJE6lHChd2R8sIZfihH2BkqxZ4JCg9HRgRQpR0sw4SJfhhT4Hs5tIZz8J+KDO+gkE4QY1GxWl4Eg+GEZplETbAdec5Q55xJCDmhKjL3QL8hofpu9MyqkAxoUOkojx/hTtHAfb/ksOK2k2TntXIy5dTosOnPKOZJCfG6e7+VTtbXHXUnMHaLkP0Hf0tDllDA4qGydiTjQDhRBJaQ0sbXhQ3hnMJuWObqG4/dsX2Fs6teZc2SypE37FfXpOT10cUWIchtRUcBp73Egvz0LCHPn/GSo+TMcfgWS+0XRAozE2waBWgcWq73ckZT/hyDD1iB0M0vjsJjMXrzP4xUjyP7kiNBkQgaBhciIWipheEL08w6W2Kv9cc//mnSwURQBgTMrqGNoybgdu1wIwDX6sw4MDB9pLclAgJY3vFydI3kv96WJaQTon+LmnpU60eXNvDYV6eIR1g5DfGSpn4yc0yitgZHMNPBgcZt4ufH/6f/EO5ycEyYAcVKltmC6Z+peUe9jZbgrNMzfczyY3ebIilwCclwknvt7mUJiGlG7tcAuEtcZZCErltoHgrXLfwONzHbASLBxXAd0iHIBvvWzcCXWrMhsTMvYuy9Yu0XUG4nv1QD53Y0xDLvOVJEd1IcEfeerFUna9bJWTtdHYyBcrWvILCUyDeC15vHS5GeKE7aSKjDGJPJgBhZT24kwYDDxWRZ0SmyzU5JiEFv1CnuXHQ7V16+Hhtwn2MisCDaAjWVHaA2dUJlmvBWzUu0efF2aNXwH+oVsOdKo/KTqiz1OvWtiiei/FoTYxJNcLyDkmoqZNApLYqtVTujmhsT2d7BTnD1QH73yHNvunkkD9EjT5iz7FVBb4qtz1WiW0w9aSULJHme7OoB3SNx3Hn6KNipg/aRwF5K3kgvM0Bbf/7VZyHddD9bU=; JSESSIONID=8614D0E3914230E097171B2F53634F70'}

if len(sys.argv) >= 2:
    file_path = sys.argv[1]

    # 读取临时文件中的JSON数据
    with open(file_path, 'r') as file:
        json_str = file.read()

    try:
        # 解析JSON字符串
        data = json.loads(json_str)

    except json.JSONDecodeError:
        print("Invalid JSON string provided.")
else:
    print("Please provide a file path as an argument.")


def send_get_request(url, params):
    response = requests.get(url, headers=headers, params=params)
    return response.text


def send_post_request(url, params, headers, data):
    response = requests.post(url, params=params, headers=headers, json=data)
    return response.text

if method == 'GET':
    response_get = send_get_request(url, data["params"])
    print(response_get)
else:
    response_post = send_post_request(url, data["params"], headers, data["body"])
    print(response_post)
```

2. 集成mock.js功能，响应内容可以动态生成，需要有node环境，安装全局依赖 `npm install mockjs -g`
新增请求响应示例，描述信息需要携带mock关键字 比如测试mock动态生成数据
响应内容填写如下 更多用法参考 http://mockjs.com/examples.html
```
{
  "data|1-10": [
    {
      "name|+1": "@ip"
    }
  ]
}
```
